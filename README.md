# Coupon : Phase II REST

## This repository includes REST services for Coupon project described in [Phase I](https://bitbucket.org/michael_klimenko_/coupons) ###

#### What is this repository for? ####

Coupon management system is intended to be a proof of concept for themes learned at Java Course.
It is the second part of Coupon project, and it introduces "RESTfull services" 

###Phase II includes the following concepts:

* Servlets
* Web.xml
* Annotations
* Injections
* Filters
* Binders
* Providers
* Recources
* SecurityContext
* 

The following repos are part of this project:

* [Coupon Project Phase I](https://bitbucket.org/michael_klimenko_/coupons)
* [Coupon Project Phase II](https://bitbucket.org/michael_klimenko_/couponsystem)

===================


### How do I get set up? ###

# Dependencies

* Development
-[JDK 1.8](https://java.com)

* Build and dependency management
-[Apache Maven](https://maven.apache.org/)

* Test Framework 
-[Junit Surefire plugin](http://maven.apache.org/surefire/maven-surefire-plugin/examples/junit.html)

* Configuration
-[Hocon](https://github.com/typesafehub/config)

* Security
-[Json Web Token](https://mvnrepository.com/artifact/com.nimbusds/nimbus-jose-jwt)

* Bussiness Logic and Database Layer
-[Coupon Project Phase I](https://bitbucket.org/michael_klimenko_/coupons)

# Configuration
all settings are in `application.conf` file

# Database configuration
run `Tester.java` to initialize DB with mock-up values from `InitDB.java` class.

# Deployment instructions
deploy on tomcat

# Running Tests
TBD
>running `build` in Phase I Coupons will fire tests.
>>Note: DB populated with slightly different values for JUnit tests.
>>in order to get mock-up values for testing REST or UI (Phase II), need to run `Tester.java`.


### Who do I talk to? ###

* michael.klimenko@gmail.com