package com.michael.coupon.web.security;

import com.michael.coupon.web.utils.exceptions.LoginResponse;
import com.michael.coupons.CouponSystem;
import com.michael.coupons.facades.ICouponClientFacade;
import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.facades.CompanyFacade;
import com.michael.coupons.facades.CustomerFacade;
import static com.michael.coupons.utils.ApplicationUtilities.log;
import com.michael.coupons.utils.Exceptions;
import javax.ejb.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Web REST Login Service provides access for users.
 *
 * @author michael
 */
@Singleton
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LoginService {

    // =======================CONSTANTS=========================
    private static final long serialVersionUID = 1L;
    
    private static final String JWT_HEADER = "X-JWT-Assertion"; 
    // =======================PARAMETERS========================
    @Context
    private HttpServletRequest request;
    


    // =======================METHODS===========================
    @POST
    @Path("/login")
    public Response login(User user) {

        //Security token will be generated upon successful login
        String token;
        //Read "Bearer XXX" authorization header if present
        String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        
        //TODELETE:  used to work with session before JWT 
        //HttpSession session = request.getSession();
        //UserInfo userInfo;
        
        
        ICouponClientFacade clientFacade;
        
        LoginResponse jsonResult = new LoginResponse();
        //JSONObject jsonResult = new JSONObject();
        
        //if (session.getAttribute("userInfo") == null) {
        
        if (authorizationHeader == null) {
            
            clientFacade = CouponSystem.getInstance().login(
                    user.getUsername(),
                    user.getPassword(),
                    user.getClientType(),
                    true
                    );

            if (clientFacade != null) {

                //Holds client ID
                Long myId;

                switch (user.getClientType()) {

                    case CUSTOMER:
                        myId = ((CustomerFacade) clientFacade)
                                .getCustomerByName(user.getUsername()).getId();
                        break;
                    case COMPANY:
                        myId = ((CompanyFacade) clientFacade)
                                .getCompanyByName(user.getUsername()).getId();
                        break;
                    default:
                        myId = 0L;
                        break;
                }

//                userInfo = new UserInfo(myId, user.getUsername(), user.getClientType(), clientFacade);
//                session.setAttribute("userInfo", userInfo);
//                session.setMaxInactiveInterval(30 * 60);


                JWTGenerator jwtGenerator = new JWTGenerator(); 
                try {
                    token = jwtGenerator.generateJWT(user, myId);
                    log("Generated token : " + token);
                    //String token = JWTGenerator.generateJWT(user,clientFacade);
                } catch (Exception ex) {
                    throw new ApplicationException(Exceptions.INVALID_LOGIN_TYPE, ex);
                }
                
                jsonResult.setAllowed(true);
                jsonResult.setResult(token);
                return Response.ok(jsonResult).header(JWT_HEADER, token).build();
                
            } else {
                
                jsonResult.setAllowed(false);
                jsonResult.setResult("Login details are incorrect : " + user.toString());
                return Response.status(Response.Status.UNAUTHORIZED).entity(jsonResult).build(); 
                
            }
            
        } else {
            
            //final UserInfo userInfo = (UserInfo) securityContext.getUserPrincipal();
            //userInfo = (UserInfo) session.getAttribute("userInfo");
            
                jsonResult.setAllowed(false);
                jsonResult.setResult("Authorization header is already present, please logout first");
                return Response.status(Response.Status.EXPECTATION_FAILED).entity(jsonResult).build(); 
        }

        
        

        //return Response.status(200).entity(jsonResult.toString()).build();
        
    }

    //TODELETE: Logout method was used with session before JWT - not needed anymore
//    @POST
//    @Path("/logout")
//    public Response logout(@CookieParam("JSESSIONID") String jSessId) {
//
//        String result = "";
//
//        if (jSessId != null) {
//
//            result += "Terminated: JSESSIONID=" + jSessId;
//
//        }
//
//        //invalidate the session if exists
//        HttpSession session = request.getSession(false);
//
//        if (session != null) {
//            //result += " clientFacade="+session.getAttribute("clientFacade");
//            session.invalidate();
//        }
//        JSONObject jsonResult = new JSONObject();
//        jsonResult.put("result", result);
//
//        return Response.status(200).entity(jsonResult.toString()).build();
//
//    }

    
}
