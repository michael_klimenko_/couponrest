package com.michael.coupon.web.security;


import com.michael.coupons.facades.ICouponClientFacade;
import com.michael.coupons.utils.ClientType;
import java.security.Principal;

/**
 * UserInfo hold currently logged user to be injected as SecurityContext
 * @author michael
 */
public class UserInfo implements Principal {
    
    private final Long myId;
    private final String name;
    private final ClientType role;
    private final ICouponClientFacade clientFacade;

    @Override
    public String getName() {
        return name;
    }

    public ClientType getRole() {
        return role;
    }

    public Long getMyId() {
        return myId;
    }
    

    public ICouponClientFacade getClientFacade() { return this.clientFacade; }

    public UserInfo(Long myId, String name, ClientType role, ICouponClientFacade clientFacade) {
        this.myId = myId;
        this.name = name;
        this.role = role;
        this.clientFacade = clientFacade;
    }
}