package com.michael.coupon.web.security;

import static com.michael.coupons.utils.ApplicationUtilities.log;
import com.michael.coupons.utils.ClientType;
import java.security.Principal;
import javax.ws.rs.core.SecurityContext;


/**
 * Implements {@link SecurityContext} to create a custom UserInfo context from JWT token.
 */
public class UserSecurityContext implements SecurityContext {

    private final UserInfo principal;
    private final boolean isSecure;
    private final ClientType role;

    /**
     * Init context.
     *
     * @param principal The {@link UserInfo}
     * @param isSecure true if secure, false otherwise
     */
    public UserSecurityContext(final UserInfo principal, final boolean isSecure) {
        this.principal = principal;
        this.isSecure = isSecure;
        this.role = principal.getRole();
       
        log("UserSecurityContext() - principal: {" + principal + "}, role: {" + role + "}, isSecure: {" + isSecure + "}");
    }

    /*
         * (non-Javadoc)
         * @see javax.ws.rs.core.SecurityContext#getAuthenticationScheme()
     */
    @Override
    public String getAuthenticationScheme() {
        return "JWT"; // informational
    }

    /*
         * (non-Javadoc)
         * @see javax.ws.rs.core.SecurityContext#getUserPrincipal()
     */
    @Override
    public Principal getUserPrincipal() {
        return principal;
    }

    /*
         * (non-Javadoc)
         * @see javax.ws.rs.core.SecurityContext#isSecure()
     */
    @Override
    public boolean isSecure() {
        return isSecure;
    }

    /*
         * (non-Javadoc)
         * @see javax.ws.rs.core.SecurityContext#isUserInRole(java.lang.String)
     */
    @Override
    public boolean isUserInRole(final String role) {
        return this.role.name().equals(role);
    }

    /* (non-Javadoc)
         * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("UserSecurityContext {")
                .append("principal:").append(principal).append(",")
                .append("roles:").append(role).append(",")
                .append("isSecure:").append(isSecure)
                .append("}");
        return builder.toString();
    }
}

